// I believe this is the way you wanted me to do it
const a ={
    key1: "string",
    key2: 125,
    key3: {
        nestedKey: 'string'
    },
    key4: [{arrayNestedObj:"test"}, [12,23]]
};

function arrayCopy(arr) {
    let copyArray = [];
    arr.forEach(elem => {
        if(Array.isArray(elem)){
            copyArray.push(arrayCopy(elem));
        }
        else if(isObject(elem)){
            copyArray.push(objCopy(elem));
        }
        else{
            copyArray.push(elem)
        }
    });

    return copyArray;
}

function isObject (value) {
    return value && typeof value === 'object' && value.constructor === Object;
}

function objCopy(obj) {
    let copyObj ={};
    for(let key in obj){
        if(Array.isArray(obj[key])){
            copyObj[key] = arrayCopy(obj[key])
        }
        else if(isObject(obj[key])){
            copyObj[key] = objCopy(obj[key]);
        }
        else{
            copyObj[key] = obj[key];
        }
    }
    return copyObj;
}

let b = objCopy(a);
console.log(b);

//this is another way that came to my mind

let c = JSON.parse(JSON.stringify(a));
console.log(c);
