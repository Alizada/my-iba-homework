let f0 = parseInt(prompt("please enter f0"));
let f1 = parseInt(prompt("please enter f1"));
let n = parseInt(prompt("Please enter which fibo number you want?"));
let result = fibo(n);

function fibo(n){
    if (n===0){
        return f0;
    }
    else if(n===1){
        return f1;
    }
    else if(n>1){
        return fibo(n-1) + fibo(n-2);
    }
    else if(n<0){
        return fibo(n+2) - fibo(n+1);
    }
}

console.log(result);
