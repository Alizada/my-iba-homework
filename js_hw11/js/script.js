enterButton = document.getElementById("enterButton");
sButton= document.getElementById("sButton");
eButton= document.getElementById("eButton");
oButton= document.getElementById("oButton");
nButton= document.getElementById("nButton");
lButton= document.getElementById("lButton");
zButton= document.getElementById("zButton");
allButtons = document.querySelectorAll(".btn");

document.addEventListener('keyup', event =>{
    switch (event.key) {
        case 'Enter':
            classHandler(enterButton);
            break;
        case 's':
            classHandler(sButton);
            break;
        case 'e':
            classHandler(eButton);
            break;
        case 'o':
            classHandler(oButton);
            break;
        case 'n':
            classHandler(nButton);
            break;
        case 'l':
            classHandler(lButton);
            break;
        case 'z':
            classHandler(zButton);
            break;
    }
});

function classHandler(key) {
    allButtons.forEach((btn)=>{
        btn.classList.remove('blueBG');
    });
    key.classList.add('blueBG');
}

