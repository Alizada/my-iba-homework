forEach() method of array executes provided function for each element of array.
For example:

let arr = ['a', 'b', 'c'];

arr.forEach(function(element) {
  console.log(element);
});

for(let element of arr){
    console.log(element);
}

This two code pieces do the same thing, but forEach is ready method, so it's simpler to use ready method