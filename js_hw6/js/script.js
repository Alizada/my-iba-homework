function filterBy(arr, type){
    return arr.filter((value) =>(typeof value)!==type);
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));