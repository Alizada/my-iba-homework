const $buttonUP = $("<button class='button-up'>To Top</button>");
$("script:first").before($buttonUP);
$buttonUP.hide();
const screenHeight = $(window).height();

$buttonUP.click(function () {
    $('html').animate({
        scrollTop: 0
    }, 1000);
});

const $navBar = $('<nav class="navbar navbar-page">\n' +
    '    <a href="#latestNews" class="navbar-links navbar-links-page">The Latest News</a>\n' +
    '    <a href="#mpp" class="navbar-links navbar-links-page">Most Popular posts</a>\n' +
    '    <a href="#ompc" class="navbar-links navbar-links-page">Our Most Popular CLients</a>\n' +
    '    <a href="#tr" class="navbar-links navbar-links-page">Top Rated</a>\n' +
    '    <a href="#hn" class="navbar-links navbar-links-page">Hot News</a>\n' +
    '</nav>');
$("script:first").before($navBar);
$navBar.hide();

$(window).scroll(function (event) {
    if ($(window).scrollTop() > screenHeight) {
        $buttonUP.fadeIn();
    } else {
        $buttonUP.fadeOut();
    }
    if ($(window).scrollTop() > 74) {
        $navBar.fadeIn();
    } else {
        $navBar.fadeOut();
    }
});

$navBar.click(function (event) {
    let section = event.target.href.split('#');
    let position = $(`#${section[section.length - 1]}`).offset().top;
    $('html').animate({
        scrollTop: position
    }, 1000);
});

$(".mpp-toggle").click(function () {
    if($(".mpp-content").is(":hidden"))
    {
        $(".mpp-toggle").text( "Hide this section" );
    }
    else {
        $(".mpp-toggle").text ("Show this section" );
    }
   $(".mpp-content").slideToggle();
});