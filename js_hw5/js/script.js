function CreateUser() {
    this.name = prompt("Please enter your name:");
    this.surname = prompt("Please enter your surname:");
    this.birthdate = stringIntoDate(prompt("Please enter your birthdate:", "dd.mm.yyyy"));
    this.getLogin = () => this.name[0].toLowerCase() + this.surname.toLowerCase();
    this.getAge = () => {
        let mils = new Date() - this.birthdate; //from this moment of time we substract birthday, thisgives us millisecond
        let difference = new Date(mils); //we create new date with these milliseconds
        return (difference.getFullYear() - 1970); //we transform milliseconds to years and substract 1970, because js turns starts counting milliseconds from 1970
    };
    this.getPassword = () => this.name[0].toUpperCase() + this.surname.toLowerCase() + this.birthdate.getFullYear();
    return this;
}

function stringIntoDate(str) {
    let parts = str.split(".");
    return new Date(parts[2], parts[1] - 1 , parts[0]);
}


const user1 = new CreateUser();
console.log("User is:");
console.log(user1);
console.log(`user is ${user1.getAge()} years old`);
console.log("User login is:");
console.log(user1.getLogin());
console.log("User password is:");
console.log(user1.getPassword());
