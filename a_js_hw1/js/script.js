/**
 *
 * @constructor
 * @param size        size of the hamburger
 * @param stuffing    selected stuffing
 * @throws {HamburgerException}  In case of incorrect usage
 */
function Hamburger(size, stuffing) {
    try {
        if (size === undefined || stuffing === undefined) {
            throw new HamburgerException("size or stuffing is not defined, you need to declare both of them!");
        } else {
            this.toppings = [];
            this.size = this.checkSize(size);
            this.stuffing = this.checkStuffing(stuffing);
        }
    } catch (e) {
        console.error(e.message);
    }
}

/* Sizes, types of stuffings and toppings */
Hamburger.SIZE_SMALL = {
    price: 50,
    cal: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    cal: 40
};
Hamburger.STUFFING_CHEESE = {
    price: 10,
    cal: 20
};
Hamburger.STUFFING_SALAD = {
    price:20,
    cal:5
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    cal: 10
};
Hamburger.TOPPING_MAYO = {
    price:20,
    cal:5
};
Hamburger.TOPPING_SPICE = {
    price:15,
    cal:0
};

/**
 * Add topping to hamburger. Several toppings can be added, only if they are different. You can't add same topping two times.
 *
 * @param topping     type of topping
 * @throws {HamburgerException}  in case of incorrect usage
 */
Hamburger.prototype.addTopping = function (topping) {
    try {
        topping = this.checkTopping(topping);
        if (this.toppings.some(el => el === topping)) {
            throw new HamburgerException("topping already exists");
        } else {
            this.toppings.push(topping);
        }
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Delete the toppping, only if it was added earlier.
 *
 * @param topping   topping type
 * @throws {HamburgerException}  in case of incorrect usage
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (this.toppings.some(el => el === topping)) {
            this.toppings.splice(this.toppings.indexOf(topping), 1);
        } else {
            throw new HamburgerException("topping does not exists, to delete");
        }
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Get list of toppings
 *
 * @return {Array} an Array with the list of constants like Hamburger.TOPPING_* inside
 */
Hamburger.prototype.getToppings = function () {
    return this.toppings;
};

Hamburger.prototype.checkTopping = function (topping) {
    switch (topping) {
        case Hamburger.TOPPING_MAYO:
            return Hamburger['TOPPING_MAYO'];
        case Hamburger.TOPPING_SPICE:
            return Hamburger['TOPPING_SPICE'];
        default:
            throw new HamburgerException("Incorrect Topping value");
    }
};
Hamburger.prototype.checkSize = function(size){
    switch (size) {
        case Hamburger.SIZE_LARGE:
            return Hamburger['SIZE_LARGE'];
        case Hamburger.SIZE_SMALL:
            return Hamburger['SIZE_SMALL'];
        default:
            throw new HamburgerException("Incorrect size value");
    }
};
Hamburger.prototype.checkStuffing = function(stuffing){
    switch (stuffing) {
        case Hamburger.STUFFING_CHEESE:
            return Hamburger['STUFFING_CHEESE'];
        case Hamburger.STUFFING_POTATO:
            return Hamburger['STUFFING_POTATO'];
        case Hamburger.STUFFING_SALAD:
            return Hamburger['STUFFING_SALAD'];
        default:
            throw new HamburgerException("Incorrect stuffing value");
    }
};
/**
 * Find out the size of the Hamburger
 */
Hamburger.prototype.getSize = function (){
    return this.size;
};

/**
 * Find out the stuffing of the Hamburger
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

/**
 * Find out the price of the hamburger
 * @return {Number} the number of price in AZN
 */
Hamburger.prototype.calculatePrice = function () {
    return this.size.price + this.stuffing.price + this.getToppings().reduce((acc, currVal) => (acc + currVal.price), 0);
};

/**
 * Find out callories amount of the hamburger
 * @return {Number} Number of calories
 */
Hamburger.prototype.calculateCalories = function () {
    return this.size.cal + this.stuffing.cal + this.getToppings().reduce((acc, currVal) => (acc + currVal.cal), 0);
};

/**
 * Provides information about an error while working with a hamburger.
 * Details are stored in the message property.
 * @constructor
 */
// function HamburgerException(msg) {
//     this.message = msg;
// }


// small hamburger with cheese
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// mayo topping
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// asking the number of calories
console.log("Calories: %f", hamburger.calculateCalories());
// asking the price
console.log("Price: %f", hamburger.calculatePrice());
// I've changed my mind, and I've decided to add more topping
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// Did hte price changed?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// How large is this hamburger
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Remove the topping
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1

// have not passed on the necessary parameters
var h2 = new Hamburger(); // => HamburgerException: no size given

// pass incorrect values, an topping instead of a size
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// add to many toppings
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'
