/**
 *
 * @constructor
 * @param size        size of the hamburger
 * @param stuffing    selected stuffing
 * @throws {HamburgerException}  In case of incorrect usage
 */
function Hamburger(size, stuffing) {
    var toppings = [];
    this.size = this.getSize(size);
    this.stuffing = this.getStuffing(stuffing);

    this.getToppingsStrings = () => {
        return [...toppings]; // safe because we are passing new array
        // return toppings;   not safe because we are passing actual array
    };
    this.setTopping = (newToppings) =>{
        if(!(newToppings instanceof Array)){
            throw new HamburgerException('newTopping is not array');
        }
        toppings.splice(
            0,
            toppings.length,
            ...newToppings
        );
    };
}

/* Sizes, types of stuffings and toppings */
Hamburger.SIZE_SMALL = {
    price: 50,
    cal: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    cal: 40
};
Hamburger.STUFFING_CHEESE = {
    price: 10,
    cal: 20
};
Hamburger.STUFFING_SALAD = {
    price:20,
    cal:5
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    cal: 10
};
Hamburger.TOPPING_MAYO = {
    price:20,
    cal:5
};
Hamburger.TOPPING_SPICE = {
    price:15,
    cal:0
};

/**
 * Add topping to hamburger. Several toppings can be added, only if they are different. You can't add same topping two times.
 *
 * @param topping     type of topping
 * @throws {HamburgerException}  in case of incorrect usage
 */
Hamburger.prototype.addTopping = function (topping){
    try{
        var currentToppings = this.getToppingsStrings();
        if(currentToppings.some(el => el === topping)){
            throw new HamburgerException("topping already exists");
        }
        else {
            currentToppings.push(topping);
            this.setTopping(currentToppings);
        }
    }
    catch (e) {
        console.error(e.message);
    }
};

/**
 * Delete the toppping, only if it was added earlier.
 *
 * @param topping   topping type
 * @throws {HamburgerException}  in case of incorrect usage
 */
Hamburger.prototype.removeTopping = function (topping) {
    try{
        var currentToppings = this.getToppingsStrings();
        if(currentToppings.some(el => el === topping)){
            currentToppings.splice(currentToppings.indexOf(topping), 1);
            this.setTopping(currentToppings);
        }
        else {
            throw new HamburgerException("topping does not exists, to delete");
        }
    }
    catch (e) {
        console.error(e.message);
    }
};

/**
 * Get list of toppings
 *
 * @return {Array} an Array with the list of constants like Hamburger.TOPPING_* inside
 */
Hamburger.prototype.getToppings = function (){
    var toppingsStrings = this.getToppingsStrings();
    return toppingsStrings.map(topping => this.checkTopping(topping))
};

Hamburger.prototype.checkTopping = function(topping){
    switch (topping) {
        case "mayo":
            return Hamburger['TOPPING_MAYO'];
        case "spice":
            return Hamburger['TOPPING_SPICE'];
        default:
            throw new HamburgerException("Incorrect Topping value");
    }
};
/**
 * Find out the size of the Hamburger
 */
Hamburger.prototype.getSize = function (size){
    switch (size.toLowerCase()) {
        case "large":
            return Hamburger.SIZE_LARGE;
        case "small":
            return Hamburger.SIZE_SMALL;
        default:
            throw new HamburgerException("Size is not right!");

    }
};

/**
 * Find out the stuffing of the Hamburger
 */
Hamburger.prototype.getStuffing = function (stuffing) {
    switch (stuffing.toLowerCase()) {
        case "cheese":
            return Hamburger.STUFFING_CHEESE;
        case "salad":
            return Hamburger.STUFFING_SALAD;
        case "potatoes":
            return Hamburger.STUFFING_POTATO;
        default:
            throw new HamburgerException("Stuffing is not right is not right!");

    }
};



/**
 * Find out the price of the hamburger
 * @return {Number} the number of price in AZN
 */
Hamburger.prototype.calculatePrice = function (){
    return this.size.price + this.stuffing.price + this.getToppings().reduce((acc, currVal) => (acc + currVal.price), 0);
};

/**
 * Find out callories amount of the hamburger
 * @return {Number} Number of calories
 */
Hamburger.prototype.calculateCalories = function (){
    return this.size.cal + this.stuffing.cal + this.getToppings().reduce((acc, currVal) => (acc + currVal.cal), 0 )
};

/**
 * Provides information about an error while working with a hamburger.
 * Details are stored in the message property.
 * @constructor
 */
function HamburgerException (msg) {
    this.message = msg;
}

const hamburger = new Hamburger('small', 'cheese');

console.log("toppings before");
console.table(hamburger.getToppings());

hamburger.addTopping("mayo");

console.log("toppings after");
// console.table(hamburger.getToppings());
//
hamburger.removeTopping("mayo");

console.log("toppings very after");
console.table(hamburger.getToppings());

hamburger.removeTopping("spice");

console.log("toppings very after");
console.table(hamburger.getToppings());

console.log(hamburger.calculateCalories());
console.log(hamburger.calculatePrice());