class Hamburger {
    /*******CONSTANTS*******/
    static get SIZE_SMALL() {
        return {
            objName: "SIZE_SMALL",
            price: 50,
            cal: 20
        }
    }

    static get SIZE_LARGE() {
        return {
            objName: "SIZE_LARGE",
            price: 100,
            cal: 40
        }
    };

    static get STUFFING_CHEESE() {
        return {
            objName: "STUFFING_CHEESE",
            price: 10,
            cal: 20
        }
    }

    static get STUFFING_SALAD() {
        return {
            objName: "STUFFING_SALAD",
            price: 20,
            cal: 5
        }
    }

    static get STUFFING_POTATO() {
        return {
            objName: "STUFFING_POTATO",
            price: 15,
            cal: 10
        }

    }

    static get TOPPING_MAYO() {
        return {
            objName: "TOPPING_MAYO",
            price: 20,
            cal: 5
        }
    }

    static get TOPPING_SPICE() {
        return {
            objName: "TOPPING_SPICE",
            price: 15,
            cal: 0
        }
    }

    /**********end************/

    set size(s) {
        switch (JSON.stringify(s)) {
            case JSON.stringify(Hamburger.SIZE_LARGE): {
                this._size = Hamburger['SIZE_LARGE'];
                break;
            }

            case JSON.stringify(Hamburger.SIZE_SMALL): {
                this._size = Hamburger['SIZE_SMALL'];
                break;
            }

            default:
                throw new HamburgerException("Incorrect size value");
        }
    }

    set stuffing(s) {
        switch (JSON.stringify(s)) {
            case JSON.stringify(Hamburger.STUFFING_CHEESE): {
                this._stuffing = Hamburger['STUFFING_CHEESE'];
                break;
            }

            case JSON.stringify(Hamburger.STUFFING_POTATO): {
                this._stuffing = Hamburger['STUFFING_POTATO'];
                break;
            }

            case JSON.stringify(Hamburger.STUFFING_SALAD): {
                this._stuffing = Hamburger['STUFFING_SALAD'];
                break;
            }

            default:
                throw new HamburgerException("Incorrect stuffing value");
        }
    }

    get size() {
        return this._size;
    }

    get stuffing() {
        return this._stuffing;
    }

    get toppings() {
        return this._toppings;
    }

    constructor(size, stuffing) {
        try {
            if (size === undefined || stuffing === undefined) {
                throw new HamburgerException("size or stuffing is not defined, you need to declare both of them!");
            } else {
                this._toppings = [];
                this.size = size;
                this.stuffing = stuffing;
            }
        } catch (e) {
            console.error(e.message);
        }
    }

    addTopping(topping){
        try {
            topping = this.checkTopping(topping);
            if (this.toppings.some(el => JSON.stringify(el) === JSON.stringify(topping))) {
                throw new HamburgerException("topping already exists");
            } else {
                this.toppings.push(topping);
            }
        } catch (e) {
            console.error(e.message);
        }
    }

    removeTopping(topping){
        try {
            if (this.toppings.some(el => JSON.stringify(el) === JSON.stringify(topping))) {
                this.toppings.splice(this.toppings.findIndex(arrEl => topping.objName === arrEl.objName), 1);
            } else {
                throw new HamburgerException("topping does not exists, to delete");
            }
        } catch (e) {
            console.error(e.message);
        }
    }

    checkTopping(topping){
        switch (JSON.stringify(topping)) {
            case JSON.stringify(Hamburger.TOPPING_MAYO):
                return Hamburger['TOPPING_MAYO'];
            case JSON.stringify(Hamburger.TOPPING_SPICE):
                return Hamburger['TOPPING_SPICE'];
            default:
                throw new HamburgerException("Incorrect Topping value");
        }
    }

    calculatePrice(){
        return this.size.price + this.stuffing.price + this.toppings.reduce((acc, currVal) => (acc + currVal.price), 0);
    }

    calculateCalories(){
        return this.size.cal + this.stuffing.cal + this.toppings.reduce((acc, currVal) => (acc + currVal.cal), 0);
    }
}

function HamburgerException(msg) {
    this.message = msg;
}


const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(hamburger);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(`Price is ${hamburger.calculatePrice()} \nCalories - ${hamburger.calculateCalories()}`);