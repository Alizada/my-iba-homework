const priceInput = document.getElementById("priceInput");
const inputContainer = document.querySelector(".price");

priceInput.addEventListener("blur", () => {
        // priceInput.style.outline = 'none';
        let inputValue = priceInput.value;
        if (inputValue > 0) {
            const currentPrice = document.createElement("span");
            currentPrice.innerHTML = `Current Price: ${inputValue} <span id="closeButton">x</span>`;
            currentPrice.classList.add("spans");
            inputContainer.before(currentPrice);
            const closeButton = document.getElementById("closeButton");
            closeButton.classList.add("close-button");
            closeButton.addEventListener("click", () => {
                const spans = document.querySelector(".spans");
                spans.remove();
                priceInput.value = 0;
                priceInput.focus();
            });
            priceInput.style.color = "green";
            priceInput.style.outline = "none";
        } else if (inputValue !== "") {
            const warning = document.createElement("span");
            warning.innerText = "Please enter correct price";
            inputContainer.after(warning);
            priceInput.style.outline = "2px solid red";
        }
    }
);

priceInput.addEventListener("focus", () => {
    priceInput.style.outline = "2px solid green";
    priceInput.style.color = "black";
    const spans = document.querySelectorAll("span");
    spans.forEach(span => {
        span.remove();
    });
});

