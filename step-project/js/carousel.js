$('.feedback-carousel-big').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.feedback-carousel-little'
});
$('.feedback-carousel-little').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.feedback-carousel-big',
    dots: true,
    centerMode: true,
    focusOnSelect: true,
    variableWidth: true,
});