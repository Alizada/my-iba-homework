//*********************Our Services Starts*********************//

function SingleTab(imgUrl, text, category = 'no category') {
    this.imgUrl = imgUrl;
    this.text = text;
    this.category = category;
    this.showTab = function () {
        const tab = document.querySelector('.tabs-content');
        //easy way udaleniya
        tab.innerHTML = "";
        const tabPhoto = document.createElement("img");
        tabPhoto.classList.add("tabs-content-photo");
        tabPhoto.src = this.imgUrl;

        const tabText = document.createElement("p");
        tabText.classList.add("tabs-content-text");
        tabText.innerText = `${this.text}`;

        tab.append(tabPhoto, tabText);
    };
    this.deleteTab = function () {
        //hard way naxodim vnutri div photo i element i proveraem eto li to cto nado udalit i udalaem
        //tab.find(elem => elem.);
        //implement deleting the tab with this content from the page
    }
}

const CATEGORIES = [
    'Web Design',
    'Graphic Design',
    'Online Support',
    'App Design',
    'Online Marketing',
    'Seo Service'
];

const TABS_CONTENT = [
    new SingleTab('img/our%20services/web-design.png',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus nobis numquam perspiciatis porro sit voluptatibus. Consectetur laboriosam minus rerum. Accusamus at, atque beatae consequatur eius error esse expedita in laboriosam, nam, nisi omnis quibusdam saepe suscipit veritatis voluptas voluptate. A alias aperiam asperiores aspernatur atque aut beatae blanditiis consequuntur deserunt, dicta dignissimos eaque eligendi, explicabo in inventore ipsa ipsam itaque maiores maxime pariatur placeat quaerat recusandae rerum temporibus, voluptates. Architecto cupiditate dolore enim laboriosam laudantium nihil officiis porro unde voluptatem. Ab amet deserunt, fugiat impedit numquam pariatur repudiandae tempore. Ad delectus eligendi enim hic in ipsam nemo nisi odit quis.',
        CATEGORIES[0]
    ),
    new SingleTab('img/graphic%20design/graphic-design1.jpg',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus nobis numquam perspiciatis porro sit voluptatibus. Consectetur laboriosam minus rerum. Accusamus at, atque beatae consequatur eius error esse expedita in laboriosam, nam, nisi omnis quibusdam saepe suscipit veritatis voluptas voluptate. A alias aperiam asperiores aspernatur atque aut beatae blanditiis consequuntur deserunt, dicta dignissimos eaque eligendi, explicabo in inventore ipsa ipsam itaque maiores maxime pariatur placeat quaerat recusandae rerum temporibus, voluptates. Architecto cupiditate dolore enim laboriosam laudantium nihil officiis porro unde voluptatem. Ab amet deserunt, fugiat impedit numquam pariatur repudiandae tempore. Ad delectus eligendi enim hic in ipsam nemo nisi odit quis.',
        CATEGORIES[1]
    ),
    new SingleTab('img/graphic%20design/graphic-design2.jpg',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus nobis numquam perspiciatis porro sit voluptatibus. Consectetur laboriosam minus rerum. Accusamus at, atque beatae consequatur eius error esse expedita in laboriosam, nam, nisi omnis quibusdam saepe suscipit veritatis voluptas voluptate. A alias aperiam asperiores aspernatur atque aut beatae blanditiis consequuntur deserunt, dicta dignissimos eaque eligendi, explicabo in inventore ipsa ipsam itaque maiores maxime pariatur placeat quaerat recusandae rerum temporibus, voluptates. Architecto cupiditate dolore enim laboriosam laudantium nihil officiis porro unde voluptatem. Ab amet deserunt, fugiat impedit numquam pariatur repudiandae tempore. Ad delectus eligendi enim hic in ipsam nemo nisi odit quis.',
        CATEGORIES[2]
    ),
    new SingleTab('img/graphic%20design/graphic-design3.jpg',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus nobis numquam perspiciatis porro sit voluptatibus. Consectetur laboriosam minus rerum. Accusamus at, atque beatae consequatur eius error esse expedita in laboriosam, nam, nisi omnis quibusdam saepe suscipit veritatis voluptas voluptate. A alias aperiam asperiores aspernatur atque aut beatae blanditiis consequuntur deserunt, dicta dignissimos eaque eligendi, explicabo in inventore ipsa ipsam itaque maiores maxime pariatur placeat quaerat recusandae rerum temporibus, voluptates. Architecto cupiditate dolore enim laboriosam laudantium nihil officiis porro unde voluptatem. Ab amet deserunt, fugiat impedit numquam pariatur repudiandae tempore. Ad delectus eligendi enim hic in ipsam nemo nisi odit quis.',
        CATEGORIES[3]
    ),
    new SingleTab('img/graphic%20design/graphic-design4.jpg',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus nobis numquam perspiciatis porro sit voluptatibus. Consectetur laboriosam minus rerum. Accusamus at, atque beatae consequatur eius error esse expedita in laboriosam, nam, nisi omnis quibusdam saepe suscipit veritatis voluptas voluptate. A alias aperiam asperiores aspernatur atque aut beatae blanditiis consequuntur deserunt, dicta dignissimos eaque eligendi, explicabo in inventore ipsa ipsam itaque maiores maxime pariatur placeat quaerat recusandae rerum temporibus, voluptates. Architecto cupiditate dolore enim laboriosam laudantium nihil officiis porro unde voluptatem. Ab amet deserunt, fugiat impedit numquam pariatur repudiandae tempore. Ad delectus eligendi enim hic in ipsam nemo nisi odit quis.',
        CATEGORIES[4]
    ),

    new SingleTab('img/graphic%20design/graphic-design5.jpg',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus nobis numquam perspiciatis porro sit voluptatibus. Consectetur laboriosam minus rerum. Accusamus at, atque beatae consequatur eius error esse expedita in laboriosam, nam, nisi omnis quibusdam saepe suscipit veritatis voluptas voluptate. A alias aperiam asperiores aspernatur atque aut beatae blanditiis consequuntur deserunt, dicta dignissimos eaque eligendi, explicabo in inventore ipsa ipsam itaque maiores maxime pariatur placeat quaerat recusandae rerum temporibus, voluptates. Architecto cupiditate dolore enim laboriosam laudantium nihil officiis porro unde voluptatem. Ab amet deserunt, fugiat impedit numquam pariatur repudiandae tempore. Ad delectus eligendi enim hic in ipsam nemo nisi odit quis.',
        CATEGORIES[5]
    )
];

const ourServicesButtons = document.querySelector(".tabs-btns");

/*** Creating a tab buttons ***/
CATEGORIES.forEach((tabName) => {
    const button = document.createElement("button");
    button.innerText = `${tabName}`;
    button.classList.add("tabs-buttons-design");
    ourServicesButtons.append(button);
});

ourServicesButtons.addEventListener('click', (event) => {
//    finding the tab to be shown
    const button = document.querySelectorAll(".tabs-buttons-design");
    button.forEach((tab) => {
        tab.classList.remove("tabs-buttons-design-active");
    });
    TABS_CONTENT.find((tab) => event.target.textContent === tab.category).showTab();
    event.target.classList.add("tabs-buttons-design-active")
});

const defaultOurServices = document.querySelector(".tabs-buttons-design");
defaultOurServices.click();

//*********************Our Services ENDS*********************//


//*********************Our Amazing Word Starts*********************//

//OAW means Our Amazing Work

const CATEGORIES_OAW = [
    'All',
    'Graphic Design',
    'Web Design',
    'Landing Pages',
    'Wordpress'
];


function SingleTab_OAW(imgUrl, category = 'no category', visible = false) {
    this.imgUrl = imgUrl;
    this.category = category;
    this.visible = visible;
    this.showTab = function () {
        this.elem = document.createElement("div");
        this.elem.classList.add("our-aw-tabs-content-item");
        const container = document.querySelector('.our-aw-tabs-content');

        const hover = document.createElement("div");
        hover.classList.add("our-aw-tabs-content-item-hover");
        hover.innerHTML+='<i class="fas fa-link hover-icon"></i><i class="fas fa-search hover-icon"></i>';

        const hoverHeader = document.createElement("h4");
        hoverHeader.classList.add("hover-header");
        hoverHeader.innerText="Creative Design";
        hover.append(hoverHeader);

        const hoverText = document.createElement('p');
        hoverText.classList.add("hover-text");
        hoverText.innerText=this.category;
        hover.append(hoverText);

        this.elem.append(hover);

        const photo = document.createElement("img");
        photo.src = this.imgUrl;
        this.elem.append(photo);
        container.append(this.elem)
    };
    this.deleteTab = function () {
        //hard way naxodim vnutri div photo i element i proveraem eto li to cto nado udalit i udalaem
        //tab.find(elem => elem.);
        //implement deleting the tab with this content from the page
    }
}

const TABS_CONTENT_OAW = [
    new SingleTab_OAW("img/graphic%20design/graphic-design1.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design2.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design3.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design4.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design5.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design6.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design7.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design8.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design9.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design10.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design11.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/graphic%20design/graphic-design12.jpg", CATEGORIES_OAW[1], false),
    new SingleTab_OAW("img/web%20design/web-design1.jpg", CATEGORIES_OAW[2], false),
    new SingleTab_OAW("img/web%20design/web-design2.jpg", CATEGORIES_OAW[2], false),
    new SingleTab_OAW("img/web%20design/web-design3.jpg", CATEGORIES_OAW[2], false),
    new SingleTab_OAW("img/web%20design/web-design4.jpg", CATEGORIES_OAW[2], false),
    new SingleTab_OAW("img/web%20design/web-design5.jpg", CATEGORIES_OAW[2], false),
    new SingleTab_OAW("img/web%20design/web-design6.jpg", CATEGORIES_OAW[2], false),
    new SingleTab_OAW("img/web%20design/web-design7.jpg", CATEGORIES_OAW[2], false),
    new SingleTab_OAW("img/landing%20page/landing-page1.jpg", CATEGORIES_OAW[3], false),
    new SingleTab_OAW("img/landing%20page/landing-page2.jpg", CATEGORIES_OAW[3], false),
    new SingleTab_OAW("img/landing%20page/landing-page3.jpg", CATEGORIES_OAW[3], false),
    new SingleTab_OAW("img/landing%20page/landing-page4.jpg", CATEGORIES_OAW[3], false),
    new SingleTab_OAW("img/landing%20page/landing-page5.jpg", CATEGORIES_OAW[3], false),
    new SingleTab_OAW("img/landing%20page/landing-page6.jpg", CATEGORIES_OAW[3], false),
    new SingleTab_OAW("img/landing%20page/landing-page7.jpg", CATEGORIES_OAW[3], false),
    new SingleTab_OAW("img/wordpress/wordpress1.jpg", CATEGORIES_OAW[4], false),
    new SingleTab_OAW("img/wordpress/wordpress2.jpg", CATEGORIES_OAW[4], false),
    new SingleTab_OAW("img/wordpress/wordpress3.jpg", CATEGORIES_OAW[4], false),
    new SingleTab_OAW("img/wordpress/wordpress4.jpg", CATEGORIES_OAW[4], false),
    new SingleTab_OAW("img/wordpress/wordpress5.jpg", CATEGORIES_OAW[4], false),
    new SingleTab_OAW("img/wordpress/wordpress6.jpg", CATEGORIES_OAW[4], false),
    new SingleTab_OAW("img/wordpress/wordpress7.jpg", CATEGORIES_OAW[4], false),
    new SingleTab_OAW("img/wordpress/wordpress8.jpg", CATEGORIES_OAW[4], false),
    new SingleTab_OAW("img/wordpress/wordpress9.jpg", CATEGORIES_OAW[4], false),
    new SingleTab_OAW("img/wordpress/wordpress10.jpg", CATEGORIES_OAW[4], false)
];

const ourAmazingWorkButtons = document.querySelector(".our-aw-tabs-btns");

CATEGORIES_OAW.forEach((tabName) => {
    const button = document.createElement("button");
    button.innerText = `${tabName}`;
    button.classList.add("oaw-btn");
    ourAmazingWorkButtons.append(button);
});

let loadMoreCounter = 0;

function cleanOAW() {
    const container = document.querySelector('.our-aw-tabs-content');
    container.innerHTML = "";
    const loadMore = document.querySelector(".oaw-load-more");
    if (loadMore !== null) {
        loadMore.remove();
        loadMoreCounter = 0;
    }
    TABS_CONTENT_OAW.forEach(tab => {
        tab.visible = false;
    })
}

function loadMoreEvent(event) {
    loadMoreCounter++;
    let showCounter = 0;
    const show = TABS_CONTENT_OAW.filter((tab, index, array) => {
        if (showCounter === 12) {
            return false;
        }
        if (tab.visible === false) {
            showCounter++;
            tab.visible = true;
            return tab;
        }
    });
    show.splice(0, 12).forEach(e => {
        e.showTab();
    });
    if (loadMoreCounter === 3) {
        event.target.remove();
        loadMoreCounter = 0;
    }
}


ourAmazingWorkButtons.addEventListener('click', (event) => {
    const buttonsOAW = document.querySelectorAll(".oaw-btn");
    // TABS_CONTENT_OAW.forEach((tab)=>{
    //     tab.visible = false;
    //     console.log(tab);
    // });
    if (event.target.textContent === "All") {
        if (!event.target.classList.contains("oaw-btn-active")) {
            cleanOAW();
            buttonsOAW.forEach((btn) => {
                btn.classList.remove("oaw-btn-active");
            });
            event.target.classList.add("oaw-btn-active");
            const showElement = new Set();
            let countGD = 0, countWD = 0, countLP = 0, countWordpress = 0;
            for (let i = 1; i < CATEGORIES.length - 1; i++) {
                const categoryTabs = TABS_CONTENT_OAW.filter((tab, index, array) => {
                    return tab.category === CATEGORIES_OAW[i]
                });
                categoryTabs.splice(0, 3).forEach(e => {
                    e.visible = true;
                    e.showTab();
                })
            }
            const loadMore = document.createElement("button");
            loadMore.classList.add("oaw-load-more");
            loadMore.addEventListener('click', (event) =>{
                loadMore.disabled = true;
                const tabsContent = document.querySelector(".our-aw-tabs-content");
                const loading = document.createElement("div");
                loading.classList.add("load-wrapp");
                loading.innerHTML="<div class=\"load-5\"><div class=\"ring-2\"><div class=\"ball-holder\"><div class=\"ball\"></div></div></div></div>";
                tabsContent.after(loading);
                setTimeout(()=>{
                    loading.remove();
                    loadMoreEvent(event);
                    loadMore.disabled = false;
                }, 2000);
            });
            loadMore.innerHTML = '<p class="oaw-load-more-plus">+</p>Load More';
            document.querySelector(".our-aw-tabs-content").after(loadMore);
            loadMoreCounter = 1;
        }
    }
    else if (event.target.textContent === "Web Design" || event.target.textContent === "Graphic Design" || event.target.textContent === "Landing Pages" || event.target.textContent === "Wordpress") {
        cleanOAW();
        buttonsOAW.forEach((btn) => {
            btn.classList.remove("oaw-btn-active");
        });
        event.target.classList.add("oaw-btn-active");
        const showElements = TABS_CONTENT_OAW.filter((tab) => {
                if (event.target.textContent === tab.category)
                    return tab;
            }
        );
        console.log(showElements);
        showElements.forEach((tab) => {
            tab.showTab();
        });
    }
});


const defaultOAW = document.querySelector(".oaw-btn");
defaultOAW.click();
//*********************Our Amazing Word ENDS*********************//
