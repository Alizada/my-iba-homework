const burger = document.querySelector(".navbar__burger");

function burgerEvent(){
    burger.classList.toggle("change");
    const navbarUL = document.querySelector('.navbar-links');
    navbarUL.classList.toggle('navbar-links-active-mobile');
    navbarUL.style.top = document.querySelector(".navbar").clientHeight + "px";
}

burger.addEventListener('click', burgerEvent);
