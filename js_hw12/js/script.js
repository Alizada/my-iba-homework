const img = document.querySelectorAll(".image-to-show");
let currentPhoto = 0;

let carousel = setIntervalAndExecute(photoChanger, 5000);


function photoChanger() {
    img.forEach((photo,index) =>{
        photo.hidden = index !== currentPhoto;
    });

    currentPhoto++;
    if(currentPhoto === img.length){
        currentPhoto=0;
    }
}

function setIntervalAndExecute(fn, interval) {
    fn();
    return setInterval(fn, interval);
}

const stopButton = document.getElementById("stopButton");
const resumeButton = document.getElementById("resumeButton");

stopButton.addEventListener('click', (event)=> {
    if(stopButton.dataset.status === '0'){
        clearInterval(carousel);
        console.log(carousel);
        stopButton.dataset.status = '1';
        resumeButton.dataset.status = '0'
    }
});

resumeButton.addEventListener('click',(event)=>{
    if(resumeButton.dataset.status === '0'){
        carousel = setInterval(photoChanger, 5000);
        resumeButton.dataset.status = '1';
        stopButton.dataset.status = '0';
    }
});