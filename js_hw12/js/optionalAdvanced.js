const spanTimer = document.createElement("span");
spanTimer.innerText = "10";
document.querySelector(".buttons").prepend(spanTimer);
const img = document.querySelectorAll(".image-to-show");
let currentPhoto = 0;

let i = 10;
let carousel = setIntervalAndExecute(photoChanger, 10000);

let timer = setIntervalAndExecute(secondChanger, 1000);

function secondChanger(){
    spanTimer.innerText = `${i}`;
    if (i === 0) {
        clearInterval(timer);
    }
    i--;
}

function photoChanger() {
    img.forEach((photo,index) =>{
        photo.hidden = index !== currentPhoto;
    });

    currentPhoto++;
    if(currentPhoto === img.length){
        currentPhoto=0;
    }
    i = 10;
}

function setIntervalAndExecute(fn, interval) {
    fn();
    return setInterval(fn, interval);
}

const stopButton = document.getElementById("stopButton");
const resumeButton = document.getElementById("resumeButton");

stopButton.addEventListener('click', (event)=> {
    if(stopButton.dataset.status === '0'){
        clearInterval(carousel);
        clearInterval(timer);
        console.log(carousel);
        spanTimer.innerText = 'Timer is off';
        stopButton.dataset.status = '1';
        resumeButton.dataset.status = '0'
    }
});

resumeButton.addEventListener('click',(event)=>{
    if(resumeButton.dataset.status === '0'){
        carousel = setInterval(photoChanger, 10000);
        i=10
        timer = setIntervalAndExecute(secondChanger, 1000);
        spanTimer.innerText = '10';
        resumeButton.dataset.status = '1';
        stopButton.dataset.status = '0';
    }
});
