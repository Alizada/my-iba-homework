//**************BASIC VERSION*****************
//
function CreateUser() {
    this.name = prompt("Please enter your name:");
    this.surname = prompt("Please enter your surname:");
    this.getLogin = () => this.name[0].toLowerCase() + this.surname.toLowerCase();
    return this;
}

const user1 = new CreateUser();
console.log("User is:");
console.log(user1);
console.log("User login is:");
console.log(user1.getLogin());


//**************ADVANCED VERSION*****************
//could not understand will be glad if you help Alexander


// function CreateUser() {
//     this.name = null;
//     this.surname = null;
//     Object.defineProperties(this, 'name', {
//         writable: false,
//         get() {
//             return this.name;
//         },
//         set(val) {
//             value: val;
//         }
//     });
//     this.getLogin = () => this.name[0].toLowerCase() + this.surname.toLowerCase();
//     return this;
// }
//
// // prompt("Please enter your name:");
// // prompt("Please enter your surname:");
//
// const user1 = new CreateUser();
// console.log("User is:");
// console.log(user1);
// console.log("User login is:");
// console.log(user1.getLogin());