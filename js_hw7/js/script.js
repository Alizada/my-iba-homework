let addUl = document.createElement("ul");
let script = document.querySelector("script");
script.before(addUl);

let unorderedList = document.querySelector("ul");


let list = ['hello', 'world', 'Baku', 'IBA Tech Academy', '2019'];

function addList(arr){
    ( arr.map( (item) => `<li>${item}</li>` ) ).forEach(item => {
        unorderedList.innerHTML+=item;
    });


    // Another way to do it, without map
    // for(listElement of arr){
    //     let addElement = document.createElement("li");
    //     addElement.textContent=listElement;
    //     console.log(addElement);
    //     unorderedList.appendChild(addElement);
    // }
}

addList(list);
