const changeTheme =  document.querySelector(".change-theme");
changeTheme.innerText=localStorage.state==='dark' ? "Light Mode" : "Dark Mode";
changeState(localStorage.state);

changeTheme.addEventListener("click", ()=>{
    localStorage.state = localStorage.state==='dark' ? "light" : "dark";
    changeTheme.innerText=localStorage.state==='dark' ? "Light Mode" : "Dark Mode";
    changeState(localStorage.state);
});

function changeState(state) {
    if (state==='dark'){
        document.body.classList.add('dark-theme');
        document.querySelector('.logo-black').classList.add('logo-link-dark');
    }
    else {
        document.body.classList.remove('dark-theme');
        document.querySelector('.logo-black').classList.remove('logo-link-dark');
    }
}